package com.example.ciness.moviedetailsscreen.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ciness.databinding.ItemGenreBinding
import com.example.ciness.moviedetailsscreen.models.moviedetails.Genre


class GenreAdapter(

) : RecyclerView.Adapter<GenreAdapter.GenreViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GenreAdapter.GenreViewHolder {
        val itemBinding =
            ItemGenreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GenreViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: GenreAdapter.GenreViewHolder, position: Int) {
        val genreItem: String = genreItemList[position].value
        holder.bind(genreItem)
    }

    override fun getItemCount(): Int {
        return genreItemList.size
    }

    private var genreItemList = listOf<Genre>()

    fun update(list: List<Genre>?) {
        if(list != null){
            genreItemList = list
            notifyDataSetChanged()
        }
    }

    inner class GenreViewHolder(private val itemGenreBinding: ItemGenreBinding) :
        RecyclerView.ViewHolder(itemGenreBinding.root) {
        fun bind(item: String) {
            itemGenreBinding.tvGenre.text = item
        }
    }
}