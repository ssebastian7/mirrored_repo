package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Star(
    val id: String,
    val name: String
)