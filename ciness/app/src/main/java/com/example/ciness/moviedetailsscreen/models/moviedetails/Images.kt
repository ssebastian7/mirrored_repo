package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Images(
    val errorMessage: String,
    val fullTitle: String,
    val imDbId: String,
    val items: List<Item>,
    val title: String,
    val type: String,
    val year: String
)