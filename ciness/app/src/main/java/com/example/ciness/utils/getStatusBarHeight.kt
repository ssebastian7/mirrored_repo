package com.example.ciness.utils

import android.graphics.Rect
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

fun Fragment.getStatusBarHeight(): Int {
    val activity = this.requireActivity()
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    return if (resourceId > 0) resources.getDimensionPixelSize(resourceId)
    else Rect().apply { activity.window.decorView.getWindowVisibleDisplayFrame(this) }.top
}

fun View.setMarginTop(marginTop: Int){
    Log.d("SOMETHING1", marginTop.toString())
    val params = layoutParams as ViewGroup.MarginLayoutParams
    params.setMargins(params.leftMargin, marginTop, params.rightMargin, params.bottomMargin)
    layoutParams = params
    Log.d("SOMETHING2", (layoutParams as ViewGroup.MarginLayoutParams).topMargin.toString())
    Log.d("SOMETHING3", (layoutParams as ViewGroup.MarginLayoutParams).bottomMargin.toString())
}