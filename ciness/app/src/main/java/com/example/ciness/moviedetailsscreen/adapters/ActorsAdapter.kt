package com.example.ciness.moviedetailsscreen.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.ciness.databinding.ItemMoviedetailsActorBinding
import com.example.ciness.moviedetailsscreen.models.moviedetails.Actor

class ActorsAdapter() :
    RecyclerView.Adapter<ActorsAdapter.ActorsViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ActorsAdapter.ActorsViewHolder {
        val actorItemBinding =
            ItemMoviedetailsActorBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ActorsViewHolder(actorItemBinding)
    }

    override fun onBindViewHolder(holder: ActorsAdapter.ActorsViewHolder, position: Int) {
        val actorItem: Actor = actorsItemList[position]
        holder.bind(actorItem)
    }

    override fun getItemCount(): Int {
        return actorsItemList.size
    }

    private var actorsItemList = listOf<Actor>()

    fun update(list: List<Actor>) {
        actorsItemList = list
        notifyDataSetChanged()
    }

    inner class ActorsViewHolder(private val itemBinding: ItemMoviedetailsActorBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(actor: Actor) {
            itemBinding.apply {
                tvActorName.text = actor.name
                tvCharacterName.text = actor.asCharacter
                ivActorImage.load(actor.image)
            }
        }
    }

}
