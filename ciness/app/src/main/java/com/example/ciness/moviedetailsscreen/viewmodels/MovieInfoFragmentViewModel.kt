package com.example.ciness.moviedetailsscreen.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ciness.moviedetailsscreen.models.moviedetails.MovieDetailsResponse
import com.example.ciness.repository.Repository
import com.example.ciness.utils.Resource
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MovieInfoFragmentViewModel (
    private val repository: Repository
): ViewModel() {

    private val _movieInfo = MutableLiveData<Resource<MovieDetailsResponse>>()
    val movieInfo = _movieInfo

    fun getMovieInfo(movieId: String) =
        viewModelScope.launch(handler) {
            withContext(Dispatchers.IO){
                _movieInfo.postValue(Resource.Loading())
                val response = repository.getMovieDetails(movieId)
                if(response.isSuccessful){
                    response.body()?.let{ result ->
                        _movieInfo.postValue(Resource.Succes(result))
                    }
                } else{
                    _movieInfo.postValue(Resource.Error(response.message()))}
            }
        }

    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.d("Network", "Caught $exception")
    }
}

