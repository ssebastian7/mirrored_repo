package com.example.ciness.utils

import android.content.Context
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.example.ciness.R

fun TextView.setDrawableAndText(drawableId: Int, text: String, context: Context){
    val drawable = ResourcesCompat.getDrawable(resources,drawableId, context.theme)
    val width = resources.getDimension(R.dimen.emptyOrErrorStateImageWidth)
    val height = resources.getDimension(R.dimen.emptyOrErrorStateImageHeight)
    drawable?.setBounds(0,0, height.toInt(),width.toInt())
    this.setCompoundDrawables(null,drawable, null, null)
    this.text = text

}