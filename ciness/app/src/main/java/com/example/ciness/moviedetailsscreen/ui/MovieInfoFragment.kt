package com.example.ciness.moviedetailsscreen.ui

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.ciness.R
import com.example.ciness.databinding.FragmentMovieInfoBinding
import com.example.ciness.moviedetailsscreen.adapters.*
import com.example.ciness.moviedetailsscreen.viewmodels.MovieInfoFragmentViewModel
import com.example.ciness.repository.Repository
import com.example.ciness.moviedetailsscreen.utils.MovieInfoFragmentViewModelFactory
import com.example.ciness.utils.Resource
import com.google.android.material.appbar.AppBarLayout
import kotlin.math.abs

class MovieInfoFragment : Fragment() {
    private lateinit var binding: FragmentMovieInfoBinding
    private lateinit var viewModel: MovieInfoFragmentViewModel
    private lateinit var genreAdapter: GenreAdapter
    private lateinit var keywordsAdapter: KeywordsAdapter
    private lateinit var actorsAdapter: ActorsAdapter
    private lateinit var movieImagesAdapter: MovieImagesAdapter
    private lateinit var similarsAdapter: SimilarsAdapter
    private lateinit var rvKeywords: RecyclerView
    private lateinit var rvGenre: RecyclerView
    private lateinit var rvActors: RecyclerView
    private lateinit var rvImages: RecyclerView
    private lateinit var rvSimilars: RecyclerView
    private val args: MovieInfoFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMovieInfoBinding.inflate(inflater)
        rvGenre = binding.rvGenre
        rvKeywords = binding.rvKeywords
        rvActors = binding.rvActors
        rvImages = binding.rvImages
        rvSimilars= binding.rvSimilars
        setupAdapters()
        binding.appBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                binding.btnAddToWatchlist.visibility = View.GONE
                binding.rootContentLayout.setPadding(
                    0,
                    requireActivity().resources.getDimensionPixelSize(R.dimen.dp16),
                    0,
                    0
                )
            } else if (verticalOffset == 0) {
                binding.btnAddToWatchlist.visibility = View.VISIBLE
                binding.rootContentLayout.setPadding(
                    0,
                    resources.getDimensionPixelSize(R.dimen.dp36),
                    0,
                    0
                )
            } else {
                //binding.btnAddToWatchlist.visibility = View.GONE
            }
        }
        )
        val repository = Repository()
        val viewModelFactory = MovieInfoFragmentViewModelFactory(repository)
        viewModel =
            ViewModelProvider(this, viewModelFactory)[MovieInfoFragmentViewModel::class.java]
        setupToolbar()
        val view = binding.root
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movieId = args.movieId
        viewModel.getMovieInfo(movieId)
        viewModel.movieInfo.observe(viewLifecycleOwner, { result ->
            when (result) {
                is Resource.Succes -> {
                    result.data?.let { movieInfo ->
                        binding.ivMovieImage.load(movieInfo.image)
                        genreAdapter.update(movieInfo.genreList)
                        keywordsAdapter.update(movieInfo.keywordList)
                        actorsAdapter.update(movieInfo.actorList)
                        movieImagesAdapter.update(movieInfo.images.items)
                        similarsAdapter.update(movieInfo.similars)
                    }

                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.movie_info_menu, menu)
        menu.findItem(R.id.add_to_watchlist).setOnMenuItemClickListener {
            it.setIcon(R.drawable.ic_save_bold)
            Toast.makeText(context, "Save", Toast.LENGTH_SHORT).show()
            return@setOnMenuItemClickListener true
        }
    }


    private fun setupToolbar() {
        binding.apply {
            setHasOptionsMenu(true)
            (activity as AppCompatActivity).setSupportActionBar(movieInfoToolbar)
        }
    }

    private fun setupAdapters() {
        keywordsAdapter = KeywordsAdapter()
        genreAdapter = GenreAdapter()
        actorsAdapter = ActorsAdapter()
        movieImagesAdapter = MovieImagesAdapter()
        similarsAdapter = SimilarsAdapter()
        rvGenre.apply {
            layoutManager = LinearLayoutManager(context, GridLayoutManager.HORIZONTAL, false)
            adapter = genreAdapter
        }
        rvKeywords.apply {
            layoutManager = LinearLayoutManager(context, GridLayoutManager.HORIZONTAL, false)
            adapter = keywordsAdapter
        }
        rvActors.apply {
            layoutManager = LinearLayoutManager(context, GridLayoutManager.HORIZONTAL, false)
            adapter = actorsAdapter
        }
        rvImages.apply {
            layoutManager = LinearLayoutManager(context, GridLayoutManager.HORIZONTAL, false)
            adapter = movieImagesAdapter
        }
        rvSimilars.apply {
            layoutManager = LinearLayoutManager(context, GridLayoutManager.HORIZONTAL, false)
            adapter = similarsAdapter
        }
    }

}