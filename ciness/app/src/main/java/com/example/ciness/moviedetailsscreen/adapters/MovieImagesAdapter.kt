package com.example.ciness.moviedetailsscreen.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.ciness.databinding.ItemMoviedetailsImageBinding
import com.example.ciness.moviedetailsscreen.models.moviedetails.Item

class MovieImagesAdapter :
    RecyclerView.Adapter<MovieImagesAdapter.MovieImagesViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MovieImagesAdapter.MovieImagesViewHolder {
        val imageItemBinding =
            ItemMoviedetailsImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieImagesViewHolder(imageItemBinding)
    }

    override fun onBindViewHolder(holder: MovieImagesAdapter.MovieImagesViewHolder, position: Int) {
        val imageItem: Item = imagesItemList[position]
        holder.bind(imageItem)
    }

    override fun getItemCount(): Int {
        return imagesItemList.size
    }

    private var imagesItemList = listOf<Item>()

    fun update(list: List<Item>) {
        imagesItemList = list
        notifyDataSetChanged()
    }

    inner class MovieImagesViewHolder(private val itemBinding: ItemMoviedetailsImageBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(image: Item) {
            itemBinding.apply {
                ivMovieImage.load(image.image)
            }
        }
    }

}
