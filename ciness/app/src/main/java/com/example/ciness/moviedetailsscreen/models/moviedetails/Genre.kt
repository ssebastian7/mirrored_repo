package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Genre(
    val key: String,
    val value: String
)