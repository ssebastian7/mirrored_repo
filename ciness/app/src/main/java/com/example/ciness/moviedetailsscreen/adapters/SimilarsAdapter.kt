package com.example.ciness.moviedetailsscreen.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.ciness.databinding.ItemSimilarBinding
import com.example.ciness.moviedetailsscreen.models.moviedetails.Similar

class SimilarsAdapter : RecyclerView.Adapter<SimilarsAdapter.SimilarsViewHolder>(){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SimilarsAdapter.SimilarsViewHolder {
        val itemBinding = ItemSimilarBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SimilarsViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: SimilarsAdapter.SimilarsViewHolder, position: Int) {
        val itemSimilar = similarItemsList[position]
        holder.bind(itemSimilar)
    }

    override fun getItemCount(): Int {
        return similarItemsList.size
    }

    var similarItemsList = listOf<Similar>()

    fun update(list: List<Similar>?){
        if( list != null){
            similarItemsList = list
            notifyDataSetChanged()
        }
    }

    inner class SimilarsViewHolder (private val itemBinding: ItemSimilarBinding):
            RecyclerView.ViewHolder(itemBinding.root){
                fun bind (similarMovie: Similar){
                    itemBinding.apply {
                        ivSimilarMovieImage.load(similarMovie.image)
                        tvMovieName.text = similarMovie.title
                    }
                }
            }
}