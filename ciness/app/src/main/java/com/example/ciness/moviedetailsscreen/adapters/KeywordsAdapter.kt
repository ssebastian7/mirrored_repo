package com.example.ciness.moviedetailsscreen.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ciness.databinding.ItemKeywordBinding


class KeywordsAdapter(

) : RecyclerView.Adapter<KeywordsAdapter.KeywordsViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): KeywordsAdapter.KeywordsViewHolder {
        val itemBinding =
            ItemKeywordBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return KeywordsViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: KeywordsAdapter.KeywordsViewHolder, position: Int) {
        val keywordItem: String = keywordsItemList[position]
        holder.bind(keywordItem)
    }

    override fun getItemCount(): Int {
        return keywordsItemList.size
    }

    private var keywordsItemList = listOf<String>()

    fun update(list: List<String>) {
        keywordsItemList = list
        notifyDataSetChanged()
    }

    inner class KeywordsViewHolder(private val itemKeywordBinding: ItemKeywordBinding) :
        RecyclerView.ViewHolder(itemKeywordBinding.root) {
        fun bind(item: String) {
            itemKeywordBinding.tvKeyword.text = item
        }
    }
}