package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Company(
    val id: String,
    val name: String
)