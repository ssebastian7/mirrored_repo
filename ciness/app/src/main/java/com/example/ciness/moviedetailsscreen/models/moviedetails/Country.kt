package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Country(
    val key: String,
    val value: String
)