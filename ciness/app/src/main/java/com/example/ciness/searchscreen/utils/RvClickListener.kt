package com.example.ciness.searchscreen.utils

import com.example.ciness.searchscreen.models.search.Result


interface RvClickListener {
    fun onClick(movie: Result, position: Int)
}