package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Writer(
    val id: String,
    val name: String
)