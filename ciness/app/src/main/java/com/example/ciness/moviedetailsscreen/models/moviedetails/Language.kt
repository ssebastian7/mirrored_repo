package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Language(
    val key: String,
    val value: String
)