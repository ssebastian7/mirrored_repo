package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Item(
    val image: String,
    val title: String
)