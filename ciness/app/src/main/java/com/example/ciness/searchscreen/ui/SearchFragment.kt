package com.example.ciness.searchscreen.ui

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ciness.searchscreen.adapters.SearchAdapter
import com.example.ciness.databinding.FragmentSearchBinding
import com.example.ciness.searchscreen.models.search.Result
import com.example.ciness.repository.Repository
import com.example.ciness.searchscreen.viewmodels.SearchFragmentViewModel
import android.view.ViewGroup
import com.example.ciness.R
import com.example.ciness.searchscreen.utils.RvClickListener
import com.example.ciness.searchscreen.utils.SearchFragmentViewModelFactory
import com.example.ciness.utils.*


class SearchFragment : Fragment(), RvClickListener {
    private lateinit var binding: FragmentSearchBinding
    private lateinit var searchView: SearchView
    private lateinit var rvSearch: RecyclerView
    private lateinit var searchAdapter: SearchAdapter
    private lateinit var viewModel: SearchFragmentViewModel
    private var isKeyboardDisplayed = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(layoutInflater)
        val view = binding.root
        val statusBarHeight = this.getStatusBarHeight()
        Log.d("SOMETHING", statusBarHeight.toString())
        //val toolbar = binding.searchToolbar
        //toolbar.setMarginTop(statusBarHeight)
        rvSearch = binding.rvSearch
        setupAdapter()
        observeKeyboard()
        binding.rootLayout.setOnClickListener {
            handleSearchViewFocus(searchView)
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        val repository = Repository()
        val viewModelFactory = SearchFragmentViewModelFactory(repository)
        viewModel = ViewModelProvider(this,viewModelFactory)[SearchFragmentViewModel::class.java]
        viewModel.resultList.observe(viewLifecycleOwner, {results ->
            when (results){
                is Resource.Loading ->{
                    binding.apply {
                        rvSearch.visibility = View.GONE
                        emptyOrErrorStateMessage.visibility = View.GONE
                        loadingCircularIndicator.visibility = View.VISIBLE
                    }
                }
                is Resource.Error ->{
                    binding.apply {
                        rvSearch.visibility = View.GONE
                        loadingCircularIndicator.visibility = View.GONE
                        emptyOrErrorStateMessage.setDrawableAndText(
                            R.drawable.ic_error_server, getString(
                                R.string.error_state_message
                            ), requireContext())
                        emptyOrErrorStateMessage.visibility = View.VISIBLE
                    }
                }
                is Resource.Succes ->{
                    results.data?.let{
                            searchAdapter.searchItemList = it
                    }
                    binding.apply {
                        emptyOrErrorStateMessage.visibility = View.GONE
                        loadingCircularIndicator.visibility = View.GONE
                        rvSearch.visibility = View.VISIBLE

                    }
                }
                is Resource.Empty ->{
                    binding.apply {
                        loadingCircularIndicator.visibility = View.GONE
                        rvSearch.visibility = View.GONE
                        emptyOrErrorStateMessage.setDrawableAndText(
                            R.drawable.ic_laptop_01, getString(
                                R.string.empty_state_message
                            ), requireContext())
                        emptyOrErrorStateMessage.visibility = View.VISIBLE
                    }
                }
            }

        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.top_app_bar_search,menu)
        searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.apply {
                queryHint = getString(R.string.search_hint)

        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                viewModel.search(query)
                searchView.clearFocus()
                return false
            }
            override fun onQueryTextChange(newText: String): Boolean {

                return false
            }
        })
    }

    private fun setupToolbar() {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).setSupportActionBar(binding.searchToolbar)
    }

    private fun setupAdapter(){
        searchAdapter = SearchAdapter(this)
        rvSearch.apply {
            layoutManager = GridLayoutManager(context, resources.getInteger(R.integer.spanCount), GridLayoutManager.VERTICAL,false)
            adapter = searchAdapter
        }
    }

    private fun handleSearchViewFocus(searchView: SearchView) {
        searchView.apply {
            if (isInTouchMode) {
                if (query.toString() != "") {
                    clearFocus()
                    hideKeyboard()
                } else {
                    clearFocus()
                    hideKeyboard()
                    isIconified = true
                }
            }
        }
    }

    override fun onClick(movie: Result, position: Int){
        if(isKeyboardDisplayed){
            searchView.hideKeyboard()
            searchView.clearFocus()
        }
        else{
            val directions = SearchFragmentDirections.actionSearchFragmentToMovieInfoFragment(movie.id)
            findNavController().navigate(directions)
        }
    }

    private fun observeKeyboard(){
        binding.apply{
            rootLayout.viewTreeObserver.addOnGlobalLayoutListener {
                val r = Rect()
                rootLayout.getWindowVisibleDisplayFrame(r)
                val heightDiff = rootLayout.rootView.height - r.height()
                isKeyboardDisplayed = heightDiff > 0.25 * rootLayout.rootView.height
            }
        }
    }



}

