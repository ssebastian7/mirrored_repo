package com.example.ciness.network

import com.example.ciness.moviedetailsscreen.models.moviedetails.MovieDetailsResponse
import com.example.ciness.searchscreen.models.search.SearchTitle
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ImdbApi {

    @GET ("/en/API/SearchTitle/{apiKey}/{expression}")
    suspend fun searchMovieOrSeries(
        @Path("apiKey") apiKey: String,
        @Path("expression") queryExpression: String
    ): Response<SearchTitle>

    @GET ("/en/API/Title/{apiKey}/{id}/Images,Trailer,Ratings")
    suspend fun getMovieDetails(
        @Path("apiKey") apiKey: String,
        @Path("id") movieId: String
    ): Response<MovieDetailsResponse>
}