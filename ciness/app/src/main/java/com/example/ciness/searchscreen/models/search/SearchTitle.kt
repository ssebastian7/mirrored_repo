package com.example.ciness.searchscreen.models.search

data class SearchTitle(
    val errorMessage: String,
    val expression: String,
    val results: List<Result>,
    val searchType: String
)