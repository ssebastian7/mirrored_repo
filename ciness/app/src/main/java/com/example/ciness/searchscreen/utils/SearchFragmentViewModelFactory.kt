package com.example.ciness.searchscreen.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.ciness.repository.Repository
import com.example.ciness.searchscreen.viewmodels.SearchFragmentViewModel

class SearchFragmentViewModelFactory(
    private val repository: Repository
): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SearchFragmentViewModel(repository) as T
    }
}