package com.example.ciness.moviedetailsscreen.models.moviedetails

data class BoxOffice(
    val budget: String,
    val cumulativeWorldwideGross: String,
    val grossUSA: String,
    val openingWeekendUSA: String
)