package com.example.ciness.searchscreen.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.ciness.R
import com.example.ciness.searchscreen.utils.RvClickListener
import com.example.ciness.databinding.ItemSearchBinding
import com.example.ciness.searchscreen.models.search.Result

class SearchAdapter(
    val onClickListener : RvClickListener
): RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchViewHolder {
        val itemBinding = ItemSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val searchItem: Result = searchItemList[position]
        holder.bind(searchItem,position)
    }

    override fun getItemCount(): Int {
        return searchItemList.size
    }

    private val diffCallback = object : DiffUtil.ItemCallback<Result>(){
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallback)

    var searchItemList: List<Result>
        get() = differ.currentList
        set(value) {differ.submitList(value)}

    inner class SearchViewHolder(private val itemSearchBinding: ItemSearchBinding) :
        RecyclerView.ViewHolder(itemSearchBinding.root){
        fun bind(item: Result, position: Int){
            itemSearchBinding.apply {
                tvTitle.text = item.title
                val texts = modifyDescription(item.description)
                tvYear.text = texts[0]
                tvType.text = texts[1]
                if(item.image.contains("nopicture.jpg")){
                    ivPoster.load(R.drawable.movie_img_placeholder)
                }
                else{
                    ivPoster.load(item.image){
                        crossfade(true)
                        placeholder(R.drawable.movie_img_placeholder)
                        error(R.drawable.movie_img_placeholder)
                    }
                }
                rootCardView.setOnClickListener {
                    onClickListener.onClick(item, position)
                }
//                btnSave.setOnClickListener {
//                    onClickListener.onClick(item, position)
//                }
            }
        }

        private fun modifyDescription(description: String): List<String> {
            var modifiedDescription = description.replace(Regex("""[()"'\\]+"""), "")
            val type = when {
                modifiedDescription.contains("TV Series") -> {
                    "TV Series"
                }
                modifiedDescription.contains("Short") -> {
                    "Short"
                }
                modifiedDescription.contains("Video") -> {
                    "Video"
                }
                else -> {
                    "Movie"
                }
            }
            modifiedDescription = modifiedDescription.replaceAfter(" ", "")
            return listOf(modifiedDescription, type)
        }

    }

}