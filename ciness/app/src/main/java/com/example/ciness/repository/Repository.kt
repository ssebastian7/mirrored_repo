package com.example.ciness.repository


import com.example.ciness.BuildConfig
import com.example.ciness.network.RetrofitInstance


class Repository {
     suspend fun searchMovieOrSeries(queryExpression: String) = RetrofitInstance.movieApi.searchMovieOrSeries(
          BuildConfig.IMDB_API_KEY3, queryExpression)

     suspend fun getMovieDetails(movieId: String) = RetrofitInstance.movieApi.getMovieDetails(
          BuildConfig.IMDB_API_KEY3, movieId
     )
}