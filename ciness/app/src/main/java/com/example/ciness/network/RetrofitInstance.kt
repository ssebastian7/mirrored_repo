package com.example.ciness.network

import com.example.ciness.BuildConfig.IMDB_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitInstance {

        private val loggingInterceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        private val client = OkHttpClient.Builder().apply {
            connectTimeout(10,TimeUnit.SECONDS)
            readTimeout(10, TimeUnit.SECONDS)
            addInterceptor(loggingInterceptor)
        }.build()

        private val retrofit by lazy{
            Retrofit.Builder()
                .client(client)
                .baseUrl(IMDB_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        val movieApi: ImdbApi by lazy {
            retrofit.create(ImdbApi::class.java)
        }

}