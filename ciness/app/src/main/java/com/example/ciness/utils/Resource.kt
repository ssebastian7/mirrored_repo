package com.example.ciness.utils

sealed class Resource <T> (
    val data : T? = null,
    val message: String? = null
) {
    class Succes<T>(data: T): Resource<T>(data)
    class Empty<T>(data: T): Resource<T>(data)
    class Error<T>(message: String, data: T? = null): Resource<T>(data, message)
    class Loading<T>: Resource<T>()
}