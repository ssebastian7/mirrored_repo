package com.example.ciness.moviedetailsscreen.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.ciness.repository.Repository
import com.example.ciness.moviedetailsscreen.viewmodels.MovieInfoFragmentViewModel

class MovieInfoFragmentViewModelFactory(
    private val repository: Repository
): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MovieInfoFragmentViewModel(repository) as T
    }
}