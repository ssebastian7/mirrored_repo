package com.example.ciness.utils

import android.widget.ProgressBar
import android.widget.RatingBar
import androidx.databinding.BindingAdapter


@BindingAdapter("android:progress")
fun setProgress(view: ProgressBar?, progress: String?) {
    if (view != null) {
        if(progress != null && progress.isNotEmpty()){
            val prog = progress.toFloat()
            if(prog < 10){
                view.progress = prog.times(10).toInt()
            }
            else{
                view.progress = prog.toInt()
            }

        }
        else{
            view.progress = 0
        }
    }
}

@BindingAdapter("android:rating")
fun setRating(view: RatingBar?, rating: String?) {
    if (view != null) {
        if( rating != null && rating.isNotEmpty()){
            val result = rating.toFloat()/2f
            view.rating = result
        }
        else{
            view.rating = 0.0f
        }
        }
    }