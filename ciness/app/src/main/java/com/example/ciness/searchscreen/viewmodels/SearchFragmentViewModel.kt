package com.example.ciness.searchscreen.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ciness.searchscreen.models.search.Result
import com.example.ciness.repository.Repository
import com.example.ciness.utils.Resource
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SearchFragmentViewModel (
    private val repository: Repository
): ViewModel() {

    private val _resultList = MutableLiveData<Resource<List<Result>>>()
    val resultList = _resultList

    private val _searchUiState = MutableLiveData<SearchUiState>()
    val searchUiState = _searchUiState

    fun search(query: String) =
        viewModelScope.launch(handler) {
            withContext(Dispatchers.IO){
            _resultList.postValue(Resource.Loading())
            val response = repository.searchMovieOrSeries(query)
            if(response.isSuccessful){
                response.body()?.let { result ->
                    when (result.results){
                        null ->{ _resultList.postValue(Resource.Error(response.message())) }
                        emptyList<Result>() ->{ _resultList.postValue(Resource.Empty(result.results)) }
                        else ->{ _resultList.postValue(Resource.Succes(result.results))}
                    }
                }
            }
            else{
                _resultList.postValue(Resource.Error(response.message()))}
            }
        }

    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.d("Network", "Caught $exception")
        _resultList.postValue(Resource.Error("Timeout"))
    }
}

sealed class SearchUiState{
    object Default : SearchUiState()
}