package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Director(
    val id: String,
    val name: String
)