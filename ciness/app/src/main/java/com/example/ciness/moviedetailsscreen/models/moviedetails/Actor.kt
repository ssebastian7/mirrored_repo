package com.example.ciness.moviedetailsscreen.models.moviedetails

data class Actor(
    val asCharacter: String,
    val id: String,
    val image: String,
    val name: String
)